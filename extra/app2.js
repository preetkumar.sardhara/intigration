const express = require('express');
const controller = require('./controllers/controller2.js')

const app = express();

// app.use(express.static('../sprinklr'));

controller(app);

app.listen(1000);
console.log('You are listening to port 1000');