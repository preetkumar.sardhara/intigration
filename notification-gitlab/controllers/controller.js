const sendMessage = require('../api/slack.js')
const urlencoded = require('body-parser');
const urlencodedParser = urlencoded({extended: false});

module.exports = function(app){

    app.route('/')
        .post(urlencodedParser, function(req, res) {
            switch(req.headers['x-github-event']) {
                case 'push':
                    push();
                    break;
                case 'tag_push':
                    tag_push();
                    break;
                case 'issue':
                    issue();
                    break;
                case 'note':
                    switch(req.object_attributes.noteable_type) {
                        case 'Commit':
                            note_commit(req.object_attributes.note);
                            break;
                        case 'MergeRequest':
                            note_mergeRequest(req.object_attributes.note);
                            break;
                        case 'Issue':
                            note_issue(req.object_attributes.note);
                            break;
                        case 'Snippet':
                            note_snippet(req.object_attributes.note);
                            break;
                    }
                    break;
                case 'merge_request':
                    merge_request();
                    //switch case to be added ( maybe )
                    break;
                case 'wiki_page':
                    switch(req.object_attributes.action) {
                        case 'create':
                            wiki_create();
                            break;
                        case 'delete':
                            wiki_delete();
                            break;
                        case 'update':
                            wiki_update();
                            break;
                    }
                    break;
                case 'pipeline':
                    pipeline();
                    //switch case to be added ( maybe )
                    break;
                case 'job':
                    job();
                    break;
            }
            res.status(200).end();
        })
}

function push() {
    sendMessage('user','preetsardhara1999','Push was made to the repo.');
    console.log('Push was made to the repo.');
};

function tag_push() {
    console.log(`Tag was created or deletd from the repository.`)
}

function issue() {
    console.log(`New issue is created or an existing issue is updated/closed/reopened.`);
}

function note_commit(comment) {
    console.log(`A new comment is made on commits. The comment is '${comment}'`);
}

function note_mergeRequest(comment) {
    console.log(`A new comment is made on merge request. The comment is '${comment}'`);
}

function note_issue(comment) {
    console.log(`A new comment is made on issue. The comment is '${comment}'`);
}

function note_snippet(comment) {
    console.log(`A new comment is made on snippet. The comment is '${comment}'`);
}

function merge_request() {
    console.log(`A merge request is made to the repository.`);
}

function wiki_create() {
    console.log('A wiki page is created.');
}

function wiki_delete() {
    console.log('A wiki page is deleted.');
}

function wiki_update() {
    console.log('A wiki page is updated.');
}

function pipeline() {
    console.log('Status of the pipeline has changed.');
}

function job() {
    console.log('Status of job has changed.');
}